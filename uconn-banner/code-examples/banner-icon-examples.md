# Icon examples

## Classic version
why provide both a title and a span? 
the title provides a tooltip on hover. the design requires an icon w/o text.
the icon is provided by the <i> tag. 
but if there were no span and the css failed, there wouldn't be anything printed to the page.

```html
<div class="icon-container" id="icon-container-search">
  <a class="btn" id="uconn-search" 
    href="https://uconn.edu/search" 
    title="Search University of Connecticut">
    <span class="no-css">Search University of Connecticut</span>
    <i class="icon-search" aria-hidden="true"></i>
  </a>
</div>
```
## Alternate/modern version
Why remove the title attribute?
The screen reader will still announce the content of the span
The style here requires a plain icon with the addition of text on focus/hover  

```html
<div class="icon-container" id="icon-container-search">
  <a class="btn" id="uconn-search" href="https://uconn.edu/search">
    <span class="no-css">Search University of Connecticut</span>
    <i class="icon-search" aria-hidden="true"></i>
  </a>
  <!-- Content provided by css -->
  <div id="uconn-search-tooltip"></div>
</div>
```