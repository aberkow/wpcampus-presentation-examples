# Color Contrast

Consider the contrast of foreground to background.

Consider contrast of similar elements with different states

For instance
- Background - #ffffff
- Text - #000e2f

vs

- Non-hovered/focused Text - #c1e8f8
- Hovered/focused Text - #ffffff

![old icon hover contrast](../images/old-icon-hover-color-contrast.png)
![new icon hover contrast](../images/new-icon-hover-contrast.png)



