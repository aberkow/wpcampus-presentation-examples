# Before and After

## pseudo selectors

- `::before` is the first child of the element. it is **before** the content
- `::after` is the last child of the element. it is **after** the content

```html
<div>
  ::before content goes here
  <!-- arbitrary content -->
  ::after content goes here
</div>
```