a counter would be helpful...
couter = 0;

start_el(&$output, $item, $depth = 0, $args = array()) {
  needs to decide - is there a top level page? let's say "no" for now.

  if (there are no children for the item) {
    the output should be a list item with link.
  } else {
    there's a top level item!
    does it have a link?
    if (nope. no link yet) {
      the output should be a button with the title of the item.
    } else {
      hey! there's a link here!
      the output should be a link with the title of the item 
        and a button as a sibling.
    }
  }
  
  add 1 to the counter.
}

