<?php
class A11y_Menu_Walker extends Walker_Nav_Menu {
  /**
   * 
   * @var int $this->parentItemCount
   *   - a way to associate groups of menu items via data attributes and id's.
   * 
   */
  public function __construct() {
    $this->parentItemCount = 0;
  }
  /**
   * 
   * @param $output - the accumulated html of the menu
   * @param $depth - the depth in the tree
   * @param $args - arguments passed to the item from wp_nav_menu
   * 
   * The levels are only the interior <ul> tags (sub-menus).
   */
  public function start_lvl(&$output, $depth = 0, $args = array()) {
    // these levels are created after the start_el function.
    $id = $this->parentItemCount - 1;
    $output .= "<ul id='list-$id' class='submenu-list'>";
  }
  /**
   * 
   * @param $output - the accumulated html of the menu
   * @param $item - the specific menu item in question
   * @param $depth - the depth in the tree
   * @param $args - arguments passed to the item from wp_nav_menu
   * 
   */
  public function start_el(&$output, $item, $depth = 0, $args = array()) {
    /** 
     * 
     * Code related to classes all comes from the WP Walker_Nav_Menu class
     * The only change is that it adds a no-js class to each item.
     * 
    */
    // will there be a submenu and is the current item a top-level item?
    $has_top_level_page = false;

    $classes = null;

    if (!empty($item->classes)) {
      $classes = (array)$item->classes;
    }
    
    $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item, $args, $depth));
    
    $class_names = $class_names ? " class='no-js " . esc_attr($class_names) . "'" : "";
    
    // check that items with children have links
    if (in_array('menu-item-has-children', $classes) && strlen($item->url) > 0) {
      $has_top_level_page = true;
    }
    
    
    if (!in_array('menu-item-has-children', $classes)) {
      // if there are no children, just output a link with the href and title
      $output .= "<li $class_names><a href='$item->url'>$item->title</a>";

    } else {
      // trying to use a data attribute to keep track of the list items with children.
      $output .= "<li data-count='$this->parentItemCount' data-has-children='true' $class_names>";
      if (!$has_top_level_page) {
        // if the thing at the top of the list doesn't go to a top level page, make it into a button.
        $output .= "<button aria-haspopup='true' aria-expanded='false' aria-owns='list-$this->parentItemCount' class='submenu-toggle'>$item->title";
        // the data-before attribute let's you change the value via js like with an event handler.
        $output .= "<span class='submenu-icon' aria-hidden='true' data-before='∨'></span></button>";
      } else {
        $output .= "<a class='submenu-link' aria-label='$item->title, tab to the next button to expand the sub-menu' href='$item->url'>$item->title</a>";
        $output .= "<button class='submenu-button submenu-toggle' 
          aria-expanded='false' 
          aria-haspopup='true' 
          aria-label='show submenu' 
          aria-owns='list-$this->parentItemCount'>
          <span aria-hidden='true' class='submenu-icon' data-before='∨'></span>
        </button>";
      }
      // build the list items then increment the count of how many contain children
      $this->parentItemCount++;
    }
  }
}