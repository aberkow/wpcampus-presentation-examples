class Navigation {
  constructor(
    {
      menuId = 'main-menu',
      fontFamily = 'Font Awesome 5 Free',
      chevronDown = '\\f078',
      chevronUp = '\\f077'
    } = {}
  ) {
    this.chevronDown = chevronDown;
    this.chevronUp = chevronUp;
    this.fontFamilies = [
      'FontAwesome', 
      'Font Awesome 5 Free', 
      'Glyphicons Halflings'
    ];
    this.fontFamily = fontFamily;
    this.hasNestedSubmenu = false;
    this.menu = null;
    this.menuId = menuId;
  }
  chevronSwitcher(element) {
    // toggles the appearance of the icon
  }

  /**
   * These three handlers mostly take care of
   *  - toggling ARIA attributes
   *  - calling the chevronSwitcher function
   *  - keeping track of which submenus should be open or closed.
   * 
   * @param {object} evt 
   *  - the DOM event object passed by the dispatcher
   */
  clickHandler(evt) {}
  focusInHandler(evt) {}
  hoverHandler(evt) {}

  eventDispatcher(evt) {
    switch (evt.type) {
      // sends the events to the appropriate handler
    }
  }
  removeClass() {
    // remove the no-js class
  }
  setEventListeners() {
    // a for loop that adds event listeners
    // it also sends the incoming events to the dispatcher
  }
  setSubmenuIcon() {
    // overrides the default icons
    // also checks that the fontFamily is valid 
  }
  init() {
    this.menu = document.getElementById(this.menuId);
    this.removeClass();
    this.setEventListeners();
    this.setSubmenuIcon();
  }
}

document.addEventListener('DOMContentLoaded', () => {
});


/**
 * Navigation takes an optional config object.
 * @param {object}
 *  - menuId - the ID of the menu as a string
 *  - fontFamily - the font-family for any font icons
 *  - chevronUp - an HTML escaped font icon
 *  - chevronDown - an HTML escaped font icon
 */
const navigation = new Navigation();
navigation.init();

